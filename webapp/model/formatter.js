sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {
		StatusColor: function (status) {
			if (status === "Completed")
				return "Success";
			else if (status === "SOS")
				return "Error";
			else if (status === "In Progress")
				return "Warning";
			else if (status === "Delayed")
				return "Information";
			else if (status === "Scheduled")
				return "Warning";

		},

		PriorityColor: function (priority) {
			if (priority === "HIGH")
				return "Error";
			else if (priority === "MEDIUM")
				return "Warning";
			else if (priority === "LOW")
				return "Success";

		}
	};
});